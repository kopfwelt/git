# Git Commit Message Convention
## Contents

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Summary](#summary)
- [The right way to do it](#the-right-way-to-do-it)
- [The idea of a commit](#the-idea-of-a-commit)
- [Content and structure of commit messages](#content-and-structure-of-commit-messages)
- [Last but not least](#last-but-not-least)
- [Good Examples of Git History Tree](#good-examples-of-git-history-tree)
- [References](#references)

<!-- /TOC -->

## Summary

![WTFs/min](http://www.osnews.com/images/comics/wtfm.jpg)

---
# The right way to do it
There is no right way to do it! 

# The idea of a commit 

## Structural split of code changes
First things first. You cant write a "good" commit message ( thus keeping your git history tree clean and understandable to others ) if you're not sure what a single commit should actually consist of.
The cardinal rule to follow for creating good commits is to ``ensure there is only one "logical change" per commit``. 

Reasons why this is an important rule:

- The smaller the amount of code being changed, the quicker & easier it is to review & identify potential flaws.

- If a change is found to be flawed later, it may be necessary to revert the broken commit. This is much easier to do if there are not other unrelated code changes entangled with the original commit.

- When troubleshooting problems, small well defined changes will aid in isolating exactly where the code problem was introduced.

- When browsing history, small well defined changes also aid in isolating exactly where & why a piece of code came from.

Another good rule to follow is :

```If a code change can be split into a sequence of patches/commits, then it should be split.```

### Important things to avoid when creating commits :

* Mixing whitespace changes with functional code changes.

* Mixing two unrelated functional changes.

* Sending large new features in a single giant commit.

More detailed version of the guideline above can be found here [here](https://wiki.openstack.org/wiki/GitCommitMessages#Structural_split_of_changes)<br>
Some bad and good examples, concerning the structural split of the code can be found [here](https://wiki.openstack.org/wiki/GitCommitMessages#Examples_of_bad_practice)

---

# Content and structure of commit messages
## Content of commit messages

``The commit message must contain all the information required to fully understand & review the commit for correctness.``

Important list with things to remember, when writing a commit message can be found [here](https://wiki.openstack.org/wiki/GitCommitMessages#Information_in_commit_messages)

Logically a commit message should consist of `Subject-`, `Body-`, and `Footer` parts, that are separeted with a blank line.

However the required contents may vary, depending on the project! <br>`Read the GIT section in the readme file included in the project!`



## Structure of commit messages
The general structure of a commit message should look like this :

```
[1]->  [<type>](<scope>) <subject>
       <BLANK LINE>
[2]->  <body>
       <BLANK LINE>
[3]->  <footer>
```

### [1] **Subject** : Short summary of changes
The Nr.1 rule to follow for the Subject is to be concise (expressing or covering much in few words). 

The subject can also include optional `[<type>]` and `(<scope>)` parameters. The allowed values for the parameters will be defined in the `readme` file.

Possible examples for `<type>` : [update], [fix], [remove], [add], [merge], [troubleshoot]

Possible examples for `<scope>` : (network), (dependencies), (javascript), (css), (buttons)

Other important rules :
- Separate subject from body with a blank line
- Try to limit the subject line to 50 characters
- Capitalize the subject line
- Do not end the subject line with a period
- Use the imperative mood in the subject line

<br>

### [2] **Body** : More detailed explanatory text, if necessary.

Use the Body to explain what you changed and why you changed it, instead of describing how you changed it. It should be wrapped at 72 characters.

Further paragraphs come after blank lines.

  - Bullet points are okay, too

  - Typically a hyphen or asterisk is used for the bullet, preceded by a
    single space, with blank lines in between, but conventions vary here

<br>

### [3] **Footer** 

Use the footer to include external links or references like Bug/Ticket tracking system. More information [here](https://wiki.openstack.org/wiki/GitCommitMessages#Including_external_references)


# Last but not least

Try to use the console for your commit messages. You can `usually` set your favourite text editor to handle the editing of the message and set a template, that will help you get in the right mindset to write a "good" message. Also try to avoid using `-m <msg> / --message=<msg>` flag to `git commit` .

To set up `atom`, for example, you can use : 
* `git config --global core.editor "atom --wait"`

To set up `Visual Studio Code`, for example, you can use : 
* `git config --global core.editor "code --wait"`

To set up a template, save the content in a file (e.g. ~/.commit_msg.txt ) and run: 
* `git config --global commit.template ~/.commit_msg.txt` 

 A good template for a commit message :

```
# [type](scope) If applied, this commit will...

# Explain why this change is being made and what it does...

# Provide links to any relevant tickets, articles or other resources
```
will look like this in the editor :

![Commit Message Template](/docs/Git/gitMSGG.png)   

----

# Good Examples of Git History Tree
 
* [Example 1](https://github.com/spring-projects/spring-boot/commits/master)
* [Example 2](https://github.com/tpope/vim-pathogen/commits/master)
* [Example 3](https://github.com/torvalds/linux/commits/master)
* [Example 4](https://github.com/git/git/commits/master)

---

## References
* [Git Commit Good Practice](https://wiki.openstack.org/wiki/GitCommitMessages)
* [5 Useful Tips For A Better Commit Message](https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message)
* [The Art of the Commit](https://alistapart.com/article/the-art-of-the-commit)
* [Proper Git Commit Messages and an Elegant Git History](http://ablogaboutcode.com/2011/03/23/proper-git-commit-messages-and-an-elegant-git-history)
* [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/#why-not-how)
* [Who-T On commit messages](http://who-t.blogspot.de/2009/12/on-commit-messages.html)
* [Karma Git Commit Msg](http://karma-runner.github.io/1.0/dev/git-commit-msg.html)
* [A useful template for commit messages](http://codeinthehole.com/tips/a-useful-template-for-commit-messages/)
* [Comprehensive Guide for Git (Pro Git book)](https://git-scm.com/book/en/v2)
